#!/bin/bash

ARGS="-Djexer.ECMA48.sixelPaletteSize=64"
ARGS="$ARGS -Djexer.ECMA48.sixelFastAndDirty=true"
ARGS="$ARGS -Djexer.ECMA48.imageThreadCount=2"
ARGS="$ARGS -Djava.awt.headless=true"

# We have to redirect stderr because MochaDoom spews lots of output to
# it.  Sigh.
java $ARGS -Djexer.Swing=false -jar build/jar/xtermdoom.jar 2>err

# We have to reset the terminal because MochaDoom may have called
# System.exit() without giving Jexer a chance to restore the terminal.
reset
