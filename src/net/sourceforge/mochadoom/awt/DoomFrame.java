package net.sourceforge.mochadoom.awt;

import net.sourceforge.mochadoom.doom.DoomMain;
import net.sourceforge.mochadoom.doom.ICommandLineManager;
import net.sourceforge.mochadoom.doom.event_t;
import net.sourceforge.mochadoom.system.DoomVideoInterface;
import net.sourceforge.mochadoom.system.IDoomSystem;
import net.sourceforge.mochadoom.system.InputListener;
import net.sourceforge.mochadoom.system.Strings;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Locale;
import java.util.StringTokenizer;
import javax.swing.JFrame;
import net.sourceforge.mochadoom.timing.ITicker;
import net.sourceforge.mochadoom.video.DoomVideoRenderer;

import xterm.DoomLoggerWindow;
import xterm.XtermDoomWindow;
import java.awt.image.BufferedImage;
import static java.awt.RenderingHints.*;

/**
 * Common code for Doom's video frames
 */
public abstract class DoomFrame<V> implements DoomVideoInterface<V> {

    private XtermDoomWindow xtermWindow;

    protected V RAWSCREEN;
    // Normally only used in fullscreen mode

    /**
     * This might differ from the raster's width & height attribute for
     * number of reasons
     */
    protected Dimension size;

    public String title = Strings.MOCHA_DOOM_TITLE;

    public DoomFrame(DoomMain<?, V> DM, DoomVideoRenderer<?, V> V) {
        this.DM = DM;
        this.CM = DM.CM;
        this.TICK = DM.TICK;
        this.I = DM.I;
        this.V = V;

        this.width = V.getWidth();
        this.height = V.getHeight();

        xtermWindow = XtermDoomWindow.getInstance();
        xtermWindow.setDoomFrame(this);
    }

    protected static final boolean D = false;

    // Must be aware of "Doom" so we can pass it event messages inside a crude queue.
    public DoomMain<?, V> DM;            // Must be aware of general status.
    public ICommandLineManager CM; // Must be aware of command line interface.
    protected IDoomSystem I;         // Must be aware of some other shit like event handler
    protected DoomVideoRenderer<?, V> V;    // Must have a video renderer....
    protected ITicker TICK;          // Must be aware of the ticker/
    protected MochaEvents eventhandler; // Separate event handler a la _D_.
    // However I won't make it fully "eternity like" yet
    // also because it works quite flakey on Linux.

    /**
     * This is the actual screen
     */
    protected Image screen;
    protected int palette = 0;

    protected Point center;

    /**
     * Dimensions of the screen buffers. The display however, might differ due
     * to e.g. letterboxing
     */
    protected int width, height;
    protected int multiply = 1;

    // This stuff should NOT get through in keyboard events.
    protected final int UNACCEPTABLE_MODIFIERS = (int) (InputEvent.ALT_GRAPH_DOWN_MASK +
            InputEvent.META_DOWN_MASK +
            InputEvent.META_MASK +
            InputEvent.WINDOW_EVENT_MASK +
            InputEvent.WINDOW_FOCUS_EVENT_MASK);

    public String processEvents() {
        StringBuffer tmp = new StringBuffer();
        event_t event;
        while ((event = InputListener.nextEvent()) != null) {
            tmp.append(event.type.ordinal() + "\n");
        }
        return tmp.toString();
    }


    /**
     * I_SetPalette
     * <p/>
     * Any bit-depth specific palette manipulation is performed by
     * the VideoRenderer. It can range from simple (paintjob) to
     * complex (multiple BufferedImages with locked data bits...ugh!
     *
     * @param palette index (normally between 0-14).
     */

    @Override
    public void SetPalette(int palette) {
        V.setPalette(palette);
        this.screen = V.getCurrentScreen();
    }


    /**
     * Call this before attempting to draw anything.
     * This will create the window, the canvas and everything.
     * Unlike a simple JFrame, this is not automatic because of the order
     * Doom does things.
     */
    @Override
    public void InitGraphics() {

        String d;
        int n;
        int pnum;
        int x = 0;
        int y = 0;

        // warning: char format, different type arg
        int xsign = ' ';
        int ysign = ' ';

        boolean oktodraw;
        long attribmask;

        // check for command-line geometry
        if ((pnum = CM.CheckParm("-geom")) != 0)
        {
            try {
                String eval = CM.getArgv(pnum + 1).trim();
                // warning: char format, different type arg 3,5
                //n = sscanf(myargv[pnum+1], "%c%d%c%d", &xsign, &x, &ysign, &y);
                // OK, so we have to read a string that may contain
                // ' '/'+'/'-' and a number. Twice.
                StringTokenizer tk = new StringTokenizer(eval, "-+ ");
                // Signs. Consider positive.
                xsign = 1;
                ysign = 1;
                for (int i = 0; i < eval.length(); i++) {
                    if (eval.charAt(i) == '-') {
                        // First '-' on trimmed string: negagive
                        if (i == 0)
                            xsign = -1;
                        else
                            ysign = -1;
                    }
                }

                //this should parse two numbers.
                if (tk.countTokens() == 2) {
                    x = xsign * Integer.parseInt(tk.nextToken());
                    y = ysign * Integer.parseInt(tk.nextToken());
                }


            } catch (NumberFormatException e) {
                I.Error("bad -geom parameter");
            }
        }

        this.eventhandler = new MochaEvents(DM);
        this.eventhandler.addEvent(MochaDoomInputEvent.GET_YOUR_ASS_OFF);
        SetGamma(0);
    }


    @Override
    public void StartFrame() {
        // Dummy. Nothing special to do...yet.
    }

    @Override
    public void StartTic() {
        //  DoomLoggerWindow.log("Getting events...");
        while (eventhandler.hasMoreEvents())
            eventhandler.GetEvent();
    }

    protected int lasttic;
    protected int frames;

    @Override
    public void UpdateNoBlit() {
    }

    // Convenience, for testers.
    public void GetEvent() {
        this.eventhandler.GetEvent();
    }

    @Override
    public void ShutdownGraphics() {
    }


    public void SetGamma(int level) {
        if (D) DoomLoggerWindow.log("Setting gamma " + level);
        V.setUsegamma(level);
        screen = V.getCurrentScreen(); // Refresh screen after change.
        RAWSCREEN = V.getScreen(DoomVideoRenderer.SCREEN_FG);
    }

    public BufferedImage getScreen() {
        V.update();
        Image frame = V.getCurrentScreen();
        if (frame instanceof BufferedImage) {
            // System.err.println("getScreen(): " + frame);
            return (BufferedImage) frame;
        }
        if (frame == null) {
            return null;
        }
        BufferedImage newImage = new BufferedImage(frame.getWidth(null),
            frame.getHeight(null), BufferedImage.TYPE_INT_RGB);
        Graphics2D gr = (Graphics2D) newImage.getGraphics();
        gr.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_OFF);
        gr.setRenderingHint(KEY_RENDERING, VALUE_RENDER_SPEED);
        gr.drawImage(frame, 0, 0, null);
        gr.dispose();
        return newImage;
    }

}
