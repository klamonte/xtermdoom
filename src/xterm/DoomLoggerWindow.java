/*
 * MochaDOOM on Xterm
 *
 * The MIT License (MIT)
 *
 * Copyright (C) 2022 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package xterm;

import jexer.TApplication;
import jexer.TText;
import jexer.TWindow;
import jexer.event.TResizeEvent;

/**
 * Display messages from the DOOM engine in a nice scrollable window.
 */
public class DoomLoggerWindow extends TWindow {

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The global instance (AAAAAHHH!) of this window.
     */
    private static DoomLoggerWindow instance = null;

    /**
     * The log lines showing on screen.
     */
    private TText text;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param application TApplication that manages this window
     */
    public DoomLoggerWindow(final TApplication application) {

        super(application, "XtermDOOM Log", 0, 0,
            application.getScreen().getWidth(),
            application.getDesktopBottom() - application.getDesktopTop(),
            RESIZABLE);

        if (instance != null) {
            // Only only instance allowed.
            remove();
            return;
        }
        instance = this;

        text = addText(String.format("Welcome to XtermDOOM! New game starting at %tc",
                System.currentTimeMillis()),
            0, 0, getWidth() - 2, getHeight() - 2, "ttext");
        text.setLineSpacing(0);
    }

    // ------------------------------------------------------------------------
    // Event handlers ---------------------------------------------------------
    // ------------------------------------------------------------------------

    @Override
    public void onResize(final TResizeEvent event) {
        if (event.getType() == TResizeEvent.Type.WIDGET) {
            TResizeEvent textSize;
            textSize = new TResizeEvent(event.getBackend(),
                TResizeEvent.Type.WIDGET, event.getWidth() - 2,
                event.getHeight() - 2);
            text.onResize(textSize);
        }
    }

    // ------------------------------------------------------------------------
    // DoomLoggerWindow -------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Get the static instance.
     *
     * @return the static instance
     */
    public static DoomLoggerWindow getInstance() {
        return instance;
    }

    /**
     * Emit a string to the log.
     *
     * @param str the string
     */
    public static void log(String str) {
        TText text = getInstance().text;

        // TODO: Strip out or process \t and \n
        while (str.endsWith("\n")) {
            str = str.substring(0, str.length() - 1);
        }
        text.addLine(str);
        text.toLeft();
        text.toBottom();
    }

    /**
     * Emit a format string and arguments to the log.
     *
     * @param format the string
     * @param args... the arguments
     */
    public static void logf(final String format, final Object... args) {
        TText text = getInstance().text;
        String str = String.format(format, args);

        // TODO: Strip out or process \t and \n
        while (str.endsWith("\n")) {
            str = str.substring(0, str.length() - 1);
        }
        text.addLine(str);
        text.toLeft();
        text.toBottom();
    }

}
